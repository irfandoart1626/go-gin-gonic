package routers

import (
	"go-gin-gonic/controllers"

	"github.com/gin-gonic/gin"
)

// "go-gin-gonic/controllers"

func StartServer() *gin.Engine {
	router := gin.Default()

	router.POST("/cars", controllers.CreateCar)

	router.PUT("/cars/:carID", controllers.UpdateCar)

	router.GET("/cars/:carID", controllers.GetCar)

	router.DELETE("/cars/:carID", controllers.DeleteCar)
	
	return router
}
